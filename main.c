#include <atmel_start.h>
#include <util/delay.h>

#include "TCS34725.h"

struct {
	uint16_t red;
	uint16_t green;
	uint16_t blue;
	uint16_t clear;
	uint16_t temperature;
	uint16_t illuminance;
} readout = {0};

void datastream(uint8_t *data, uint8_t len);

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	sei();

	tcs34725_init();
	
	//LED_set_level(true);
	tcs34725_setIntegrationTime(TCS34725_INTEGRATIONTIME_101MS);
	_delay_ms(105);

	/* Replace with your application code */
	while (1) {
		tcs34725_getRawData(&readout.red, &readout.green, &readout.blue, &readout.clear);
		readout.temperature = calculateColorTemperature_dn40(readout.red, readout.green, readout.blue, readout.clear);
		readout.illuminance = calculateLux(readout.red, readout.green, readout.blue);
		datastream((uint8_t*) &readout, sizeof(readout));
	}
}

void datastream(uint8_t *data, uint8_t len)
{
	USART_0_write(0x3c);
	while(len--) {
		USART_0_write(*data++);		
	}
	USART_0_write(~0x3c);
}