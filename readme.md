# LED Color Characterization

I want to figure out how to utilize a 5 channel RGBCCT LED strip in the best way possible, by running through a lot of combinations of colors and brightness settings to see how I can convert this into a full color/brightness thing.

Board: AVR-IoT, using an ATmega4808

Sensor: TCS34725, driver from https://github.com/adafruit/Adafruit_TCS34725

End result: An RGBCCT driver using an AVR-IoT board, talking with Home Assistant over MQTT.